package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * La clase IO se encarga de ofrecer facilidades al desarrollador respecto a input y output
 * 
 * @author (Abraham Meza Vega, Tribeth Rivas Pérez)
 * @version (1.0 01/08/2018)
 */
public class IO {
static BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    
    /**
     * Imprime cualquier tipo de dato
     * 
     * @param  Dato o texto a imprimir
     * @return N/A
     */
    public static void print(Object texto) {
        System.out.println(texto);
    }
    
    // Inputs
    /**
     * Toma la entrada del usuario como string y retorna la entrada
     * 
     * @param  String texto es el mensaje al usuario antes de leer su entrada
     * @return String con la entrada del usuario
     */
    public static String stringput(String texto){
        print(texto);
        String entrada = null;
				try {
					entrada = input.readLine();
					return entrada;
				} catch (IOException e) {
					return stringput(texto);
				}
    }
    /**
     * Toma la entrada del usuario como int y valida que lo sea, en caso de no serlo mostrará un error y seguirá preguntando hasta que se le ingrese un entero
     * 
     * @param  String texto es el mensaje al usuario antes de leer su entrada
     * @return int con la entrada del usuario
     */
    public static int intput(String texto) {
        int entrada;
        while(true) {
            try {
                print(texto);
                entrada = Integer.parseInt(input.readLine());
                break;
            }catch(Exception excepcion) {
                print("\n!Error debe ingresar solo números enteros!\n");
            }
        }
        return entrada;
    }
    /**
     * Toma la entrada del usuario como int y valida que lo sea, en caso de no serlo mostrará un error y seguirá preguntando hasta que se le ingrese un entero en el rango especificado
     * 
     * @param  String texto es el mensaje al usuario antes de leer su entrada, el mínimo a aceptar y el máximo
     * @return int con la entrada del usuario
     */
    public static int intputInRange(String texto,int min,int max){
        int entrada;
        while(true) {
            try {
                print(texto);
                entrada = Integer.parseInt(input.readLine());
                if(entrada <= min || entrada >= max) {
                    throw new java.lang.NumberFormatException() ;
                }
                break;
            }catch(Exception excepcion) {
                print("\n!Error debe ingresar solo números enteros entre "+min+" y "+max+"!\n");
            }
        }
        return entrada;
    } 
    /**
     * Toma la entrada del usuario como float y valida que lo sea, en caso de no serlo mostrará un error y seguirá preguntando hasta que se le ingrese un número
     * 
     * @param  String texto es el mensaje al usuario antes de leer su entrada
     * @return float con la entrada del usuario
     */
    public static float floatput(String texto) {
        float entrada;
        while(true) {
            try {
                print(texto);
                entrada = Float.parseFloat(input.readLine());
                break;
            }catch(Exception excepcion) {
                print("\n!Error debe ingresar solo números!\n");
            }
        }
        return entrada;
    }
    /**
     * Toma la entrada del usuario como float y valida que lo sea, en caso de no serlo mostrará un error y seguirá preguntando hasta que se le ingrese un número en el rango especificado
     * 
     * @param  String texto es el mensaje al usuario antes de leer su entrada, el mínimo a aceptar y el máximo
     * @return float con la entrada del usuario
     */
    public static float floatputInRange(String texto,double min,double max) {
        float entrada;
        while(true) {
            try {
                print(texto);
                entrada = Float.parseFloat(input.readLine());
                if(entrada <= min || entrada >= max) {
                    throw new java.lang.NumberFormatException() ;
                }
                break;
            }catch(Exception excepcion) {
                print("\n!Error debe ingresar solo números entre "+min+" y "+max+"!\n");
            }
        }
        return entrada;
    }
}
