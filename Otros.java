package util;

import java.util.Arrays;

/**
 * La clase Otros se encarga de ofrecer facilidades al desarrollador
 * 
 * @author (Abraham Meza Vega, Tribeth Rivas Pérez)
 * @version (1.0 01/08/2018)
 */

public class Otros {

    /**
     * Toma un objeto y retorna su clase
     * 
     * @param  Objecto a evaluar
     * @return class clase del objeto
     */
    public static Object type(Object var) {
        return var.getClass();
    }
    /**
     * Toma var1 y revisa si se encuentra en var2
     * 
     * @param  Variable a revisar si esta en la segunda
     * @return boolean true si está, false si no
     */
    public static boolean in(Object var1, Object var2) {
        if (type(var2) == String.class) {
            return ((String)var2).indexOf((String)var1)!=-1;
        }
        else {
            return Arrays.asList((Object[])var2).contains(var1);
        }
    }
    /**
     * Retorna una lista de enteros en forma [0,fin[
     * 
     * @param  int fin del rango
     * @return una lista de enteros en forma [0,fin[
     */
    public static Object[] range(int fin) {
        Object[] respuesta = new Object[fin];
        for(int i = 0; i<fin; i++) {
            respuesta[i] = i;
        }
        return respuesta;
    }
    /**
     * Retorna una lista de enteros en forma [inicio,fin[
     * 
     * @param  int inicio y fin del rango
     * @return Retorna una lista de enteros en forma [inicio,fin[
     */
    public static Object[] range(int inicio,int fin) {
        Object[] respuesta = new Object[fin-inicio];
        for(int i = inicio, a = 0; i<fin; i++, a++) {
            respuesta[a] = i;
        }
        return respuesta;
    }
}
