package util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * La clase UI se encarga de ofrecer facilidades de interfaz gr�fica
 * 
 * @author (Tribeth Rivas P�rez) 
 * @version (1.0 01/08/2018)
 */

public class UI {

    //GUI
    public static String PedirTexto(String tituloVentana,String peticion , String nombreBoton) {
    	JFrame f=new JFrame(tituloVentana);
		JButton b=new JButton(nombreBoton);    
		b.setBounds(100,100,140, 40);    
		JLabel label = new JLabel(peticion);
		label.setBounds(10, 10, 100, 100);
		JTextField textfield= new JTextField();
		textfield.setBounds(110, 50, 130, 30);
		f.add(textfield);
		f.add(label);
		f.add(b);    
		f.setSize(300,300);    
		f.setLayout(null);    
		f.setVisible(true);    
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
		
						//action listener
		b.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
				f.dispose();;				
		}          
		});
		return textfield.getText();
    }

}
