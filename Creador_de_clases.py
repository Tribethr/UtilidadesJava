def metodos():
    print("Creador de clases".center(50,"#"))
    print("\n\n")
    nombres = []
    atributos=[]
    nombre = input("Ingrese el nombre de la clase: ").capitalize()
    nAtributos = int(input("Ingrese la cantidad de atributos: "))
    print("Se le pedirá la declaracion de los atributos Ejemplo: nombre = '' ")
    file = open(nombre+".java","w+")
    file.write("public class "+nombre+"{\n")
    for i in range(nAtributos):
        atributo = input("Ingrese la declaracion del atributo: ")
        nombres.append(atributo.split())
        atributos.append(atributo[4:])
        file.write("    private "+join(atributo.split()[:-2]," ")+";\n")
    file.write("    //Inicializador\n    public " + nombre + "(){\n")
    for i in range(len(atributos)):
        file.write("        "+"".join(atributos[i].split()[1:])+";\n")
    file.write("    }")
    file.write("\n    //Getters y Setters\n")
    for i in range(len(nombres)):
        file.write("    public "+nombres[i][0]+" get"+nombres[i][1].capitalize()+"(){\n        return "+nombres[i][1]+";\n    }\n    public void set"+nombres[i][1].capitalize()+"("+nombres[i][0]+" p"+nombres[i][1].capitalize()+"){\n        "+nombres[i][1]+" = p"+nombres[i][1].capitalize()+";\n    }\n")
    file.write("}")
    file.close()

def join(l, sep):
    out_str = ''
    for i, el in enumerate(l):
        out_str += '{}{}'.format(el, sep)
    return out_str[:-len(sep)] 
metodos()
